package utils;
import exception.AutomationException;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import base.BaseClass;
import org.openqa.selenium.Capabilities;
import org.testng.Assert;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ObjectRepositoryHandler extends BaseClass {
	public ObjectRepositoryHandler() throws Exception {
		super();
	}
	Validations validationActionHelper = new Validations();
	private static final Properties properties_android;
	private static final Properties properties_iOS;
	private static final Properties properties_web;


	/**
	 * This constructor creates property instances using different element property
	 * files
	 *
	 * @author Athira CS
	 * @since 20-10-2023
	 *
	 * @Exception FileNotFoundException, IOException
	 *
	 */
	static {
		properties_android = new Properties();
		properties_iOS = new Properties();
		properties_web = new Properties();
		InputStream inputStream_android = null;
		InputStream inputStream_iOS = null;
		InputStream inputStream_web = null;

		try {
			ClassLoader classLoader = ObjectRepositoryHandler.class.getClassLoader();
			inputStream_android = classLoader
					.getResourceAsStream(AutomationConstants.ANDROID_OBJECT_REPO + ".properties");
			properties_android.load(inputStream_android);

			inputStream_iOS = classLoader.getResourceAsStream(AutomationConstants.IOS_OBJECT_REPO + ".properties");
			properties_iOS.load(inputStream_iOS);

			inputStream_web = classLoader
					.getResourceAsStream(AutomationConstants.WEB_OBJECT_REPO + ".properties");
			properties_web.load(inputStream_web);


		} catch (IOException | NullPointerException e) {
			log.info(AutomationConstants.OBJECT_REPO_FILES_MISSING);
			System.exit(0);
		}
			 catch (Exception e1) {
				e1.printStackTrace();
			}
		 finally {
			try {
				inputStream_web.close();
				inputStream_android.close();
				inputStream_iOS.close();
			} catch (IOException e) {
			}
		}
	}
	/**
	 * Method to read object repository
	 *
	 * @author Athira CS
	 * @since 19-10-2023
	 * @param elementName
	 * @return
	 * @throws AutomationException
	 */
	public HashMap<String, String> readObjectRepository(final WebDriver driver, final String elementName)
			throws AutomationException {

		final HashMap<String, String> locator = new HashMap<>();
		final Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();

		if (validationActionHelper.isWeb(driver)) {
			platform = "web";
		} else {
			if (cap.getCapability("platformName").toString() != null)
				if (cap.getCapability("platformName").toString()
						.equalsIgnoreCase(AutomationConstants.PLATFORM_NAME_ANDROID)
						|| cap.getCapability("platformName").toString().equalsIgnoreCase("LINUX")) {
					platform = "android";
				} else {
					platform = "ios";
				}
		}
		try {
			String locatorType = "";
			String locatorValue = "";
			if (platform.equalsIgnoreCase("web")) {
				for (String key : properties_web.stringPropertyNames()) {
					if (key.split(">")[0].trim().equals(elementName)) {
						locatorType = key.split(">")[1];
						final String locator_mobile_web = properties_web
								.getProperty(elementName + ">" + locatorType).trim();
						locatorValue = locator_mobile_web;
						locator.put(locatorType, locatorValue);
						log.info(elementName + " >>> " + locatorType + " = " + locatorValue);
					}
				}
			}

			if (platform.equalsIgnoreCase("iOS")) {
				for (String key : properties_iOS.stringPropertyNames()) {
					if (key.split(">")[0].trim().equals(elementName)) {
						locatorType = key.split(">")[1];
						final String locator_ios = properties_iOS.getProperty(elementName + ">" + locatorType).trim();
						locatorValue = locator_ios;
						locator.put(locatorType, locatorValue);
						log.info(elementName + " >>> " + locatorType + " = " + locatorValue);
					}
				}
			}

			if (platform.equalsIgnoreCase("Android")) {
				for (String key : properties_android.stringPropertyNames()) {
					if (key.split(">")[0].trim().equals(elementName)) {
						locatorType = key.split(">")[1];
						final String locator_android = properties_android.getProperty(elementName + ">" + locatorType)
								.trim();
						locatorValue = locator_android;
						locator.put(locatorType, locatorValue);
						log.info(elementName + " >>> " + locatorType + " = " + locatorValue);
					}
				}
			}

		} catch (final Exception e) {
			e.printStackTrace();
		}
		return locator;
	}
	/**
	 * Method to locate a webelement
	 *
	 * @author Athira CS
	 * @since 23-10-2023
	 *
	 * @Exception FileNotFoundException, IOException
	 *
	 */
	public void locateElement(String elementName, WebDriver driver ) throws AutomationException {
		String locator = getLocator(elementName,driver).get("locator");
		String LocatorType = getLocator(elementName,driver).get("LocatorType");
		switch (LocatorType)
		{
			case "i":
				driver.findElement(By.id(locator)).click();
				log.info("Located element with id locator");
				break;
			case "x"	:
				driver.findElement(By.xpath(locator)).click();
				log.info("Located element with xpath locator");
				break;
			case "c" :
				driver.findElement(By.className(locator)).click();
				log.info("Located element with className locator");
				break;
			case "cs" :
				driver.findElement(By.cssSelector(locator)).click();
				log.info("Located element with css locator");
				break;
			case "n" :
				driver.findElement(By.name(locator)).click();
				log.info("Located element with name locator");
				break;
			case "l" :
				driver.findElement(By.linkText(locator)).click();
				log.info("Located element with linkText locator");
				break;
			default:
				log.error("not specified the element locator with proper details");

		}

	}

	/**
	 * Method helps to send data to web element
	 *
	 * @author Athira CS
	 * @since 23-10-2023
	 * @param driver
	 * @param elementName
	 * @param Data
	 * @throws AutomationException
	 */
	public void sendElementData(String elementName, String Data, WebDriver driver ) throws AutomationException {
		String locator = getLocator(elementName,driver).get("locator");
		String LocatorType = getLocator(elementName,driver).get("LocatorType");
		WebElement 	element = null;
		switch (LocatorType)
		{
			case "i":
				element = driver.findElement(By.id(locator));
				element.click();
				element.sendKeys(Data);
				log.info("Data send to element locator");
				break;
			case "x"	:
				element = driver.findElement(By.xpath(locator));
				element.click();
				element.sendKeys(Data);
				log.info("Located element with xpath locator and send data");
				break;
			case "c"	:
				element = driver.findElement(By.className(locator));
				element.click();
				element.sendKeys(Data);
				log.info("Located element with classname locator and send data");
				break;
			case "cs"	:
				element = driver.findElement(By.cssSelector(locator));
				element.click();
				element.sendKeys(Data);
				log.info("Located element with cssSelector locator and send data");
				break;
			case "n"	:
				element = driver.findElement(By.name(locator));
				element.click();
				element.sendKeys(Data);
				log.info("Located element with classname locator and send data");
				break;
			case "l"	:
				element = driver.findElement(By.linkText(locator));
				element.click();
				element.sendKeys(Data);
				log.info("Located element with linkText locator and send data");
				break;
			default:
				log.info("not specified the element locator with proper details\"");

		}

	}
	/**
	 * Method helps to compare web element data with specified expected data
	 *
	 * @author Athira CS
	 * @since 23-10-2023
	 * @param driver
	 * @param elementName
	 * @return
	 * @throws AutomationException
	 */
	public boolean compareElementData(String elementName, WebDriver driver ) throws AutomationException {
		String locator = getLocator(elementName,driver).get("locator");
		String LocatorType = getLocator(elementName,driver).get("LocatorType");
		boolean state = false;
		switch (LocatorType)
		{
			case "i":
				driver.findElement(By.id(locator));
				log.info("Located the element using id");
				state = true;
				return state;
			case "x"	:
				driver.findElement(By.xpath(locator));
				log.info("Located the element using xpath");
				state = true;
				return state;
			case "c"	:
				driver.findElement(By.className(locator));
				log.info("Located the element using classname");
				state = true;
				return state;
			case "l":
				driver.findElement(By.linkText(locator));
				log.info("Located the element using linkText");
				state = true;
				return state;

			case "n":
				driver.findElement(By.name(locator));
				log.info("Located the element using name");
				state = true;
				return state;
			default:
				log.error("Problem in identifying the locator");

		}

		return state;
	}
	/**
	 * Method helps to set any text as bold to particular web element based on platforms
	 *
	 * @author Athira CS
	 * @since 05-11-2023
	 * @param driver
	 * @param elementName
	 * @param Data
	 * @throws AutomationException
	 */
	public void sendDataTextAsBold(String elementName,WebDriver driver,String Data) throws AutomationException {
		locateElement(elementName,driver);
		if(Platform.getCurrent().is(Platform.MAC)) {
			//initiated action class instance to perform keyboard operation to make text as bold
			new Actions(driver)
					.sendKeys(Data)
					.keyDown(Keys.COMMAND)
					.sendKeys("A")
					.keyUp(Keys.CONTROL)
					.keyDown(Keys.COMMAND)
					.sendKeys("B")
					.keyUp(Keys.CONTROL)
					.perform();
		}else {
			//initiated action class instance to perform keyboard operation to make text as bold
			new Actions(driver)
					.sendKeys(Data)
					.keyDown(Keys.CONTROL)
					.sendKeys("A")
					.keyUp(Keys.CONTROL)
					.keyDown(Keys.CONTROL)
					.sendKeys("B")
					.keyUp(Keys.CONTROL)
					.perform();
		}
	}
	/**
	 * Method helps to verify the text displayed on the UI is coming as bold or not
	 *
	 * @author Athira CS
	 * @since 05-11-2023
	 * @param driver
	 * @param elementName
	 * @throws AutomationException
	 */
	public void verifyBoldedText(String elementName,WebDriver driver) throws AutomationException {
		String locator = getLocator(elementName,driver).get("locator");
		String LocatorType = getLocator(elementName,driver).get("LocatorType");
		String fontWeight = null;
		switch (LocatorType) {
			case "i":
				fontWeight = driver.findElement(By.id(locator)).getCssValue("font-weight");
				log.debug("obtained the  element locator" + locator + "with element locator type" + LocatorType);
				break;
			case "x":
				fontWeight = driver.findElement(By.xpath(locator)).getCssValue("font-weight");
				log.debug("obtained the fontWeight with element locator" + locator + "with element locator type" + LocatorType);
				break;
			case "c":
				fontWeight = driver.findElement(By.className(locator)).getCssValue("font-weight");
				log.debug("obtained the fontWeight with element locator" + locator + "with element locator type" + LocatorType);
				break;
			case "cs":
				fontWeight = driver.findElement(By.cssSelector(locator)).getCssValue("font-weight");
				log.debug("obtained the fontWeight with element locator" + locator + "with element locator type" + LocatorType);
				break;
			case "n":
				fontWeight = driver.findElement(By.name(locator)).getCssValue("font-weight");
				log.debug("obtained the fontWeight with element locator" + locator + "with element locator type" + LocatorType);
				break;
			case "l":
				fontWeight = driver.findElement(By.linkText(locator)).getCssValue("font-weight");
				log.debug("obtained the fontWeight with element locator" + locator + "with element locator type" + LocatorType);
				break;
			default:
				log.error("not specified the element locator with proper details & unable to get font weight");
				Assert.fail("not specified the element locator with proper details & unable to get font weight");
		}
		boolean isBold = "bold".equals(fontWeight) || "bolder".equals(fontWeight) || Integer.parseInt(fontWeight) >= 700;
		if(isBold)
		{
			Assert.assertTrue(isBold);
			log.info("Provided text is bold");
		}else {
			log.error("Provided text is not bold");
			Assert.fail("Provided text is not bold");
		}
	}
	/**
	 * Method helps to extract locator & locator type from the object repository based on provided element name
	 *
	 * @author Athira CS
	 * @since 05-11-2023
	 * @param driver
	 * @param elementName
	 * @throws AutomationException
	 */
	public Map<String, String> getLocator(String elementName, WebDriver driver) throws AutomationException {

		final HashMap<String, String> data = readObjectRepository(driver, elementName);
		// Split the string by equal signs
		String[] parts = data.toString().split("=",2);
		// Get the part before the equal sign
		String locator = parts[1].replace("}", "").replace("}", "").trim();
		// Get the part before the equal sign
		String LocatorType = parts[0].replace("{", "").trim();
		Map<String,String> nameMap = new HashMap<String,String>();
		nameMap.put("locator",locator);
		nameMap.put("LocatorType",LocatorType);
		return  nameMap;
	}

}
