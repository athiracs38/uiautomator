package utils;

import exception.AutomationException;
import org.openqa.selenium.*;
import org.testng.Assert;
import base.BaseClass;

import java.util.ArrayList;
import java.util.List;

public class Validations extends BaseClass {

	Utilities utilityActionHelper = new Utilities();

	/**
	 * Method to verify whether running platform is Android native or not
	 * 
	 * @author Athira CS
	 * @since 18-10-2023
	 * @param driver
	 * @return
	 * @throws AutomationException
	 */
	public boolean isNativeAndroid(final WebDriver driver) throws AutomationException {
		boolean isAndroid = false;
		if (!isWeb(driver)) {
			final String platform = utilityActionHelper.getCapabilities(driver).getCapability("platformName")
					.toString();
			if (platform.equalsIgnoreCase("Android") || platform.equalsIgnoreCase("LINUX")) {
				isAndroid = true;
			} else {
				isAndroid = false;
			}
		}
		return isAndroid;
	}

	/**
	 * Method to verify whether running platform is iOS native or not
	 *
	 * @author Athira CS
	 * @since 18-10-2023
	 * @param driver
	 * @return
	 * @throws AutomationException
	 */
	public boolean isNativeiOS(final WebDriver driver) throws AutomationException {
		boolean isiOS = false;
		if (!isWeb(driver)) {
			final String operatingSystem = System.getProperty("os.name");
			String executionEnvironment = new DataHandler().getProperty(AutomationConstants.AUTOMATION_TEST_CONFIG,
					AutomationConstants.EXECUTION_ENVIRONMENT);
			if (executionEnvironment.equals("local")) {
				if (operatingSystem.contains(AutomationConstants.SYSTEM_OS_MAC)) {
					final String platform = utilityActionHelper.getCapabilities(driver).getCapability("platformName")
							.toString();
					if (!platform.equalsIgnoreCase("LINUX")) {
						isiOS = true;
					} else {
						isiOS = false;
					}
				}
			} else {
				if (!isWeb(driver)) {
					final String platform = utilityActionHelper.getCapabilities(driver).getCapability("platformName")
							.toString();
					if (!platform.equalsIgnoreCase("LINUX")) {
						isiOS = true;
					} else {
						isiOS = false;
					}
				}
			}
		}
		return isiOS;
	}

	/**
	 * To check the execution platform is mobile web or not
	 *
	 * @author Athira CS
	 * @since 18-10-2023
	 * @param driver
	 * @return
	 * @throws AutomationException
	 */
	public boolean isWeb(final WebDriver driver) throws AutomationException {
		boolean capFlagCheck = false;
		final Capabilities cap = utilityActionHelper.getCapabilities(driver);
			if (cap.getCapability("browserName") != null) {
				if (cap.getCapability("browserName").toString().equalsIgnoreCase("chrome")
						|| cap.getCapability("browserName").toString().equalsIgnoreCase("firefox")|| cap.getCapability("browserName").toString().equalsIgnoreCase("internet explorer")) {
					capFlagCheck = true;
				} else {
					capFlagCheck = false;
				}
			}

		return capFlagCheck;
	}

}
