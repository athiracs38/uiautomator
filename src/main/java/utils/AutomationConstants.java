package utils;

public class AutomationConstants {

	// ============Generic=====================================
	public static final String SYSTEM_OS_WIN = "Windows";
	public static final String SYSTEM_OS_MAC = "Mac";
	public static final String EXECUTION_ENVIRONMENT = "executionEnvironment";
	public static final String TEST_ENVIRONMENT = "testEnvironment";
	public static final String PROJECTNAME = "projectName";
	public static final String SCREENSHOT_ON_TEST_CASE_PASS = "screenshotOnTestCasePass";
	public static final String ANDROID_OBJECT_REPO = "./Repositories/android_object_repo";
	public static final String IOS_OBJECT_REPO = "./Repositories/ios_object_repo";
	public static final String WEB_OBJECT_REPO = "./Repositories/web_object_repo";
	public static final String AUTOMATION_TEST_CONFIG_FILE_PATH = "./src/test/resources/automation_test_config.properties";

	// ==========> Configuration files
	public static final String FRAMEWORK_CONFIG = "framework_config";
	public static final String AUTOMATION_TEST_CONFIG = "automation_test_config";


	// ============From AutomationBase=====================================

	public static final String OBJECT_REPO_FILES_MISSING = "Object repository property files missing, test execution terminated...";
	public static final String PLATFORM_NAME_ANDROID = "Android";
	public static final String PLATFORM_NAME_IOS = "iOS";
	public static final String ANDROID_APPPACKAGE = "androidAppPackage";
	public static final String ANDROID_APPACTIVITY = "androidAppActivity";

	// ===============Exception Messages
	public static final String CAUSE = "The reason of this exception is: ";
	public static final String OBJECT_NOT_FOUND = "Object not found";
	public static final String DRIVER_MISSING = "Driver is missing";
	public static final String WRONG_APP_PATH = "Wrong application path";
	public static final String APP_PACKAGE_VALIDATION_EXCEPTION = "Empty or invalid app package is given";
	public static final String APP_ACTIVITY_VALIDATION_EXCEPTION = "Empty or invalid app activity is given";

}