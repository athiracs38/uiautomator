package utils;

import exception.AutomationException;
import io.appium.java_client.AppiumBy;
import io.appium.java_client.InteractsWithApps;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.SupportsRotation;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import base.BaseClass;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

import static org.openqa.selenium.support.locators.RelativeLocator.with;

public class Utilities extends BaseClass {

	public WebDriverWait wait;
	public Random random;
	DataHandler dataHandler = new DataHandler();

	/**
	 * Helps to retrieve and return the capability details
	 * 
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param driver
	 * @return
	 */
	public Capabilities getCapabilities(final WebDriver driver) {
		final Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		return cap;
	}


	/**
	 * Method to wait for the element to be visible using the By locator
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param driver
	 * @param elementLocator
	 * @return element
	 * @throws AutomationException
	 */
	public boolean waitForElement(WebDriver driver, By elementLocator) throws AutomationException {
		boolean isElementVisible = false;
		try {
			long timeout = Long
					.parseLong(dataHandler.getProperty(AutomationConstants.FRAMEWORK_CONFIG, "SHORT_LOADING"));
			wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
			wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
			isElementVisible = true;
		} catch (Exception e) {
			try {
				long timeout = Long
						.parseLong(dataHandler.getProperty(AutomationConstants.FRAMEWORK_CONFIG, "SHORT_LOADING"));
				wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
				wait.until(ExpectedConditions.presenceOfElementLocated(elementLocator));
				isElementVisible = true;
			} catch (Exception ex) {
				throw new AutomationException(AutomationConstants.OBJECT_NOT_FOUND + elementLocator);
			}
		}
		return isElementVisible;
	}

	/**
	 * Set delay between test steps
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param delayInSeconds
	 * @throws AutomationException
	 */
	public static void delay(int delayInSeconds) throws AutomationException {
		try {
			Thread.sleep(delayInSeconds * 1000);
		} catch (Exception lException) {
			throw new AutomationException(getExceptionMessage(), lException);
		}
	}

	/**
	 * Capture the screenshot of the current screen and store into Screenshots
	 * folder in the project structure
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param driver
	 * @throws AutomationException
	 */
	public String captureScreenshot(WebDriver driver, String fileName) throws AutomationException {
		String destinationFilePath = null;
		try {
			TakesScreenshot screenShot = ((TakesScreenshot) driver);
			File sourceFile = screenShot.getScreenshotAs(OutputType.FILE);
			destinationFilePath = System.getProperty("user.dir") + "/Screenshots/" + fileName + "_" + getCurrentDate()
					+ ".jpg";
			File destinationFile = new File(destinationFilePath);
			FileUtils.copyFile(sourceFile, destinationFile);
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage(), e);
		}
		return destinationFilePath;
	}

	/**
	 * Capture the screenshot of the current screen
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param driver
	 * @throws AutomationException
	 */
	public String captureScreenshot(WebDriver driver) throws AutomationException {
		File sourceFile = null;
		try {

			Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
			if (new Validations().isNativeAndroid(driver)) {
				delay(2);
				Process process = Runtime.getRuntime()
						.exec("adb -s " + cap.getCapability("udid").toString() + " shell input keyevent 27");
				process.waitFor();
				process.destroy();
			} else if (new Validations().isNativeiOS(driver)) {
				driver.findElement(By.name("Take Picture")).click();
			} else {
				TakesScreenshot screenShot = ((TakesScreenshot) driver);
				sourceFile = screenShot.getScreenshotAs(OutputType.FILE);
			}
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage(), e);
		}
		return sourceFile.getAbsolutePath();
	}

	/**
	 * Method to get a random number between the two ranges
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param lowerBound
	 * @param upperBound
	 * @throws AutomationException
	 */
	public int getRandomNumber(int lowerBound, int upperBound) throws AutomationException {
		try {
			random = new Random();
			int randomNum = random.nextInt(upperBound - lowerBound + 1) + lowerBound;
			return randomNum;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to get a random number with the a number length mentioned
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param numberLength
	 * @throws AutomationException
	 */
	public String getRandomNumber(int numberLength) throws AutomationException {
		try {
			random = new Random();
			int randomNum = 0;
			boolean loop = true;
			while (loop) {
				randomNum = random.nextInt();
				if (Integer.toString(randomNum).length() == numberLength
						&& !Integer.toString(randomNum).startsWith("-")) {
					loop = false;
				}
			}
			String randomNumber = Integer.toString(randomNum);
			return randomNumber;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to get a random string value with the string length mentioned
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param stringLength
	 * @throws AutomationException
	 */
	public String getRandomString(int stringLength) throws AutomationException {
		try {
			String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";
			StringBuilder sb = new StringBuilder(stringLength);
			for (int i = 0; i < stringLength; i++) {
				int index = (int) (AlphaNumericString.length() * Math.random());
				sb.append(AlphaNumericString.charAt(index));
			}
			return sb.toString();
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to get a random string which has only alphabets with the string length
	 * mentioned
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param stringLength
	 * @throws AutomationException
	 */
	public String getRandomStringOnlyAlphabets(int stringLength) throws AutomationException {
		try {
			String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvxyz";
			StringBuilder sb = new StringBuilder(stringLength);
			for (int i = 0; i < stringLength; i++) {
				int index = (int) (AlphaNumericString.length() * Math.random());
				sb.append(AlphaNumericString.charAt(index));
			}
			return sb.toString();
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to get the current date
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @throws AutomationException
	 */
	public String getCurrentDate() throws AutomationException {
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy_HH-mm-ss");
			Date date = new Date();
			String filePathdate = dateFormat.format(date).toString();
			return filePathdate;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to get the current date in the date format ddMMMyyyy
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @throws AutomationException
	 */
	public String getCurrentDateInFormatddMMMyyyy() throws AutomationException {
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
			Date date = new Date();
			String filePathdate = dateFormat.format(date).toString();
			return filePathdate;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to get a current date in the date format ddMMyyyy
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @throws AutomationException
	 */
	public String getCurrentDateInFormatddMMyyyy() throws AutomationException {
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date date = new Date();
			String filePathdate = dateFormat.format(date).toString();
			return filePathdate;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to get the day from the current date
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @throws AutomationException
	 */
	public String getDayFromCurrentDate() throws AutomationException {
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy_HH-mm-ss");
			Date date = new Date();
			String filePathdate = dateFormat.format(date).toString();
			String day = filePathdate.substring(0, 2);
			return day;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to convert a double value to an Integer
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param doubleValue
	 * @throws AutomationException
	 */
	public int convertDoubleToInt(double doubleValue) throws AutomationException {
		try {
			int intValue = (int) doubleValue;
			return intValue;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to convert a float value to an Integer
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param floatValue
	 * @throws AutomationException
	 */
	public int convertFloatToInt(float floatValue) throws AutomationException {
		try {
			int intValue = (int) floatValue;
			return intValue;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to convert a string value to an Integer
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param stringValue
	 * @throws AutomationException
	 */
	public int convertStringToInt(String stringValue) throws AutomationException {
		try {
			int intValue = Integer.parseInt(stringValue);
			return intValue;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to convert a string value to a double value
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param stringValue
	 * @throws AutomationException
	 */
	public double convertStringToDouble(String stringValue) throws AutomationException {
		try {
			double doubleValue = Double.parseDouble(stringValue);
			return doubleValue;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to convert an Integer to a string value
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param intValue
	 * @throws AutomationException
	 */
	public String convertIntToString(int intValue) throws AutomationException {
		try {
			String stringValue = String.valueOf(intValue);
			return stringValue;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to convert a double value to a string value
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param doubleValue
	 * @throws AutomationException
	 */
	public String convertDoubleToString(double doubleValue) throws AutomationException {
		try {
			String stringValue = String.valueOf(doubleValue);
			return stringValue;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to convert a string value to a long value
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @throws AutomationException
	 */
	public long convertStringToLong(String stringValue) throws AutomationException {
		try {
			long longValue = Long.parseLong(stringValue);
			return longValue;
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}

	/**
	 * Method to encode any file data
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param filePath
	 * @throws AutomationException
	 */
	public String encodeFile(String filePath) throws AutomationException {
		String encodedString = null;
		try {
			byte[] fileContent = FileUtils.readFileToByteArray(new File(filePath));
			encodedString = Base64.getEncoder().encodeToString(fileContent);
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
		return encodedString;
	}

	/**
	 * Method to decode any string data
	 * 
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param dataToBeDecoded
	 * @throws AutomationException
	 */
	public String decodeString(String dataToBeDecoded) throws AutomationException {
		byte[] decodedString = null;
		try {
			decodedString = Base64.getDecoder().decode(dataToBeDecoded);
		} catch (Exception e) {
			throw new AutomationException(getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
		return decodedString.toString();
	}

	/**
	 * Method to delete file
	 *
	 * @author Athira CS
	 * @since 24-10-2023
	 * @param filePath
	 */
	public void deleteFile(String filePath) {
		try {
			FileUtils.forceDelete(new File(filePath));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
