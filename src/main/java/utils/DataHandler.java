package utils;

import exception.AutomationException;
import base.BaseClass;

import java.io.*;
import java.util.*;

public class DataHandler {
	public DataHandler() {

	}

	/**
	 * Method to get the property value from the property file
	 *
	 * @author Athira CS
	 * @since 22-10-2023
	 * @param fileName
	 * @param propertyName
	 * @return propValue
	 * @throws AutomationException
	 */
	public String getProperty(String fileName, String propertyName) throws AutomationException {
		try {
			String propValue = "";
			Properties props = new Properties();
			ClassLoader classLoader = DataHandler.class.getClassLoader();
			InputStream input = classLoader.getResourceAsStream(fileName + ".properties");
			props.load(input);
			propValue = props.getProperty(propertyName);
			return propValue;
		} catch (IOException e) {
			throw new AutomationException(
					new BaseClass().getExceptionMessage() + "\n" + AutomationConstants.CAUSE + e.getMessage());
		}
	}


}