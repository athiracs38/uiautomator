package reporting;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.aventstack.extentreports.reporter.configuration.ViewName;
public class AutomationReportGenerator
{
    public static ExtentTest test;
    public static ExtentSparkReporter htmlReporter;
    public static ExtentReports extent;
    public static void startReport() {
        // initialize the HtmlReporter
        htmlReporter = new ExtentSparkReporter(System.getProperty("user.dir") +"/test-output/testReport.html")
                .viewConfigurer()
                .viewOrder()
                .as(new ViewName[] { ViewName.DASHBOARD, ViewName.TEST,ViewName.AUTHOR,ViewName.DEVICE,ViewName.EXCEPTION,ViewName.EXCEPTION,ViewName.LOG })
                .apply();

        //initialize ExtentReports and attach the HtmlReporter
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        htmlReporter.config().setDocumentTitle("Simple Automation Report");
        htmlReporter.config().setReportName("Test Report");
        htmlReporter.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");
        htmlReporter.config().setTheme(Theme.DARK);

    }
    public static void stopReport()
     {
         extent.flush();
     }

}
