package base;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Assert;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseClass {
    public WebDriver driver;
    private static final String LOCAL_APPIUM_ADDRESS = "http://127.0.0.1:4723";
    AppiumDriverLocalService service;
    public String platform;
    public static final Logger log = Logger.getLogger(BaseClass.class);
    Properties properties;
    /**
     * Method to open session on android,iOS and web platforms
     *
     * @param platformName
     * @param browserName
     * @return
     * @author Athira CS
     * @since 23-10-2023
     */
    public void startSession(String platformName, String browserName) throws Exception {
        platform = platformName;
        getLog4jPropertyFile();
        getConfigPropertyFile();
        if (platformName.equalsIgnoreCase("iOS")) {
            setUpTest();
            log.info("Automation for iOS gets started");
            // Create desired capabilities for iOS device
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("platformName", properties.getProperty("platformiOS"));
            capabilities.setCapability("deviceName", properties.getProperty("iOSDeviceName")); // Replace with your device name
            capabilities.setCapability("bundleId", properties.getProperty("iOSBundleId"));
            capabilities.setCapability("udid", properties.getProperty("udid"));
            try {
                driver = new IOSDriver(new URL(getAppiumServerAddress() + "/wd/hub"), capabilities);
            } catch (Exception e) {
                log.error("Failed to open the appium server instance");
                e.printStackTrace();
            }
        } else if (platformName.equalsIgnoreCase("Android")) {
           // setUpTest();
            log.info("Automation for Android gets started");
            // Create desired capabilities for Android device
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("ignoreHiddenApiPolicyError", true);
            capabilities.setCapability("platformName", properties.getProperty("platformAndroid"));
            capabilities.setCapability("deviceName", properties.getProperty("androidDeviceName"));
            capabilities.setCapability("appPackage",properties.getProperty("androidAppPackage") ); // Gmail package name
            capabilities.setCapability("appActivity", properties.getProperty("androidAppActivity"));
            capabilities.setCapability("autoGrantPermissions", "true");
            log.info("set the capability for android app");
            try {
                driver = new AndroidDriver(new URL(
                        getAppiumServerAddress() + "/wd/hub"), capabilities);
                driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            } catch (Exception e) {
                log.error("Failed to open the appium server instance for android app");
                e.printStackTrace();
            }

        } else {
            log.info("Automation for web application gets started");
            openWebBrowserSession(browserName);
        }
    }
    /**
     * Method to get log4jProperty file
     *
     * @return
     * @author Athira CS
     * @modified 31-10-2023
     * @since 22-10-2023
     */
    public static void getLog4jPropertyFile()
    {
       PropertyConfigurator.configure("src/main/resources/ConfigFiles/log4j.properties");
    }
    /**
     * Method to get Automation config property file
     *
     * @return
     * @author Athira CS
     * @modified 31-10-2023
     * @since 22-10-2023
     */
    public void getConfigPropertyFile()
    {
        properties = new Properties();
        try {
            FileInputStream fileInputStream = new FileInputStream("src/main/resources/ConfigFiles/automation_test_config.properties");
            properties.load(fileInputStream);
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Method to open appium server programmatically
     *
     * @return
     * @author Athira CS
     * @modified 31-10-2023
     * @since 22-10-2023
     */
    public void setUpTest() throws Exception {
        service = AppiumDriverLocalService.buildDefaultService();
        service.start();
        if (service == null || !service.isRunning()) {
            throw new RuntimeException("An appium server node is not started!");
        }
    }
    /**
     * Method to get appiumServer Address
     *
     * @return
     * @author Athira CS
     * @modified 31-10-2023
     * @since 22-10-2023
     */
    protected String getAppiumServerAddress() {
        return LOCAL_APPIUM_ADDRESS;
    }

    /**
     * Method to get current Date and Time
     *
     * @return
     * @author Athira CS
     * @modified 10-03-2023
     * @since 22-10-2023
     */
    private String getCurrentDateAndTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat timeFormat = new SimpleDateFormat("HH-mm-ss");
        Date date = new Date();
        String currdate = dateFormat.format(date);
        String currtime = timeFormat.format(date);
        return currdate + "_" + currtime;
    }

    /**
     * Method to set driver for Mobile or Web applications
     *
     * @param driver
     * @author Athira CS
     * @since 23-10-2023
     */
    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Method to get current running driver
     *
     * @return driver
     * @author Athira cs
     */
    public WebDriver getDriver() {
        return this.driver;
    }

    /**
     * Method to get the exception message
     *
     * @return
     * @author Athira CS
     * @since 23-10-2023
     */
    public static String getExceptionMessage() {
        StringBuffer message = new StringBuffer();

        try {
            message.append("Exception in ");
            message.append(Thread.currentThread().getStackTrace()[2].getClassName());
            message.append(".");
            message.append(Thread.currentThread().getStackTrace()[2].getMethodName());
            message.append("()");
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
        }
        return message.toString();
    }

    /**
     * Method to openWebBrowserSession in different browsers based on browser name
     *
     * @return
     * @author Athira CS
     * @since 23-10-2023
     */
    public void openWebBrowserSession(String browser) {
        try {
            if (browser.equalsIgnoreCase("firefox")) {
                driver = new FirefoxDriver();
                log.info("Launched firefox browser");
            } else if (browser.equalsIgnoreCase("chrome")) {
                driver = new ChromeDriver();
                log.info("Launched chrome browser");
            } else if (browser.equalsIgnoreCase("safari")) {
                driver = new SafariDriver();
                log.info("Launched safari browser");
            } else if (browser.equalsIgnoreCase("Edge")) {
                driver = new EdgeDriver();
                log.info("Launched edge browser");
            } else if (browser.equalsIgnoreCase("InternetExplorer")) {
                driver = new InternetExplorerDriver();
                log.info("Launched InternetExplorer browser");
            } else {
                log.warn("browser information not provided properly. Please recheck your xml file");
            }
        }catch (Exception e)
        {
          e.printStackTrace();
          log.error("failed to launch browser session");
          Assert.fail("Error in opening the browser session");
        }

        // add implicit wait
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //maximize window
        driver.manage().window().maximize();
        //launch the URL
        driver.get("https://www.google.com/gmail/about/");
        log.info("Launched gmail Login URL");
    }

    /**
     * Method to close webdriver instance for all platforms
     *
     * @return
     * @author Athira CS
     * @since 23-10-2023
     */
    public void closeSession() {
        if (driver != null) {
            driver.quit();
            log.info("closed all the active instances of driver");
        }
    }
}
