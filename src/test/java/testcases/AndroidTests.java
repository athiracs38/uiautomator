package testcases;

import base.BaseClass;
import com.aventstack.extentreports.Status;
import com.bt.TestRunner;
import org.apache.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import reporting.AutomationReportGenerator;
import testhelpers.SampleTestHelper;

import static reporting.AutomationReportGenerator.extent;

public class AndroidTests extends TestRunner {
	public static final Logger log = Logger.getLogger(AndroidTests.class);

	@Test(enabled = true, description = " test case for email app login")
	public void TC001_AndroidLogin() throws Exception {
		AutomationReportGenerator.test = extent.createTest("AndroidEmailLogin", "This test case is to execute mail login functionality ");
		new SampleTestHelper().mobileLogin(driver);
		log.info("Executed test cases for TC001_Login ");
	}

	@Test(enabled = true, description = "test case to verify compose email functionality")
	public void TC002_AndroidEmailComposing() throws Exception {
		AutomationReportGenerator.test = extent.createTest("AndroidEmailComposing", "This test case is to execute mail composing functionality ");
		new SampleTestHelper().ComposeEmail(driver);
		log.info("Executed test cases for TC002_compose email functionality ");

	}
	@Test(enabled = true, description = "test case to verify email labelling")
	public void TC003_AndroidEmailLabelling() throws Exception {
		AutomationReportGenerator.test = extent.createTest("AndroidEmailLabelling", "This test case is to execute mail composing functionality ");
		new SampleTestHelper().VerifyEmailLabelling(driver);
		log.info("Executed test cases for TC003_AndroidEmailLabelling functionality ");

	}
	@AfterMethod
	public void getResult(ITestResult result) {
		if(result.getStatus() == ITestResult.FAILURE) {
			AutomationReportGenerator.test.log(Status.FAIL,result.getThrowable());
		}
		else if(result.getStatus() == ITestResult.SUCCESS) {
			AutomationReportGenerator.test.log(Status.PASS, result.getTestName());
		}
		else {
			AutomationReportGenerator.test.log(Status.SKIP, result.getTestName());
		}
	}
}
