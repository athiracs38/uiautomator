package testcases;

import com.bt.TestRunner;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import reporting.AutomationReportGenerator;
import testhelpers.SampleTestHelper;

import static reporting.AutomationReportGenerator.extent;

public class iOSTests extends TestRunner {
	public static final Logger log = Logger.getLogger(AndroidTests.class);
	@Test(enabled = true, description = " test case for email app login")
	public void TC003_iOSLogin() throws Exception {
		AutomationReportGenerator.test = extent.createTest("iOSEmailLogin", "This test case is to execute mail login functionality for iOS ");
		new SampleTestHelper().mobileLogin(driver);
		log.info("Executed test cases for TC003_iOSLogin");
	}

	@Test(enabled = true, description = "test case to verify compose email functionality")
	public void TC004_iOSCompose_Email() throws Exception {
		AutomationReportGenerator.test = extent.createTest("iOSCompose_Email", "This test case is to execute iOS_compose email functionality");
		new SampleTestHelper().ComposeEmail(driver);
		log.info("Executed test cases for TC004_iOS_compose email functionality ");

	}
}
