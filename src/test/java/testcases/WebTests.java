package testcases;

import com.aventstack.extentreports.Status;
import com.bt.TestRunner;
import org.apache.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import reporting.AutomationReportGenerator;
import testhelpers.SampleTestHelper;

import static reporting.AutomationReportGenerator.extent;

public class WebTests extends TestRunner
{
    public static final Logger log = Logger.getLogger(WebTests.class);

    @Test(enabled = true, description = " test case for web email app login")
    public void TC005_WebLogin() throws Exception {
        AutomationReportGenerator.test = extent.createTest("WebApplicationLogin", "WebApplication Email Login functionality executed");
        new SampleTestHelper().WebLogin(driver);
        log.info("Executed test cases TC005_WebLogin ");
    }
    @Test(enabled = true, description = "test case to verify web compose email functionality")
    public void TC006_WebTextFontChange() throws Exception {
        AutomationReportGenerator.test = extent.createTest("WebApplicationEmailBodyFontVerification", "Executed test case to verify user able to send bolded text in email body & able to get same bolded text in received email");
        new SampleTestHelper().ChangeFonts(driver);
        log.info("Executed test case TC006_WebTextFontChange");

    }
    @Test(enabled = true, description = "test case to verify web compose email functionality")
    public void TC007_WebComposeEmail() throws Exception {
        AutomationReportGenerator.test = extent.createTest("WebApplicationComposeEmail", "WebApplication Compose Email functionality executed");
        new SampleTestHelper().WebComposeEmail(driver);
        log.info("Executed test cases TC007_WebComposeEmail");

    }
    @Test(enabled = true, description = "test case to verify web compose email functionality")
    public void TC008_CreateLabel() throws Exception {
        AutomationReportGenerator.test = extent.createTest("WebCreateLabel", "WebApplication Create Label functionality executed");
        new SampleTestHelper().WebCreateLabel(driver);
        log.info("Executed test cases TC008_CreateLabel");

    }
    @AfterMethod
    public void getResult(ITestResult result) {
        if(result.getStatus() == ITestResult.FAILURE) {
            AutomationReportGenerator.test.log(Status.FAIL,result.getThrowable());
        }
        else if(result.getStatus() == ITestResult.SUCCESS) {
            AutomationReportGenerator.test.log(Status.PASS, result.getTestName());
        }
        else {
            AutomationReportGenerator.test.log(Status.SKIP, result.getTestName());
        }
    }


}
