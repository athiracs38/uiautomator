package testhelpers;

import exception.AutomationException;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import utils.*;

import java.awt.*;
import java.awt.font.TextAttribute;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;

public class SampleTestHelper {
    ObjectRepositoryHandler objr = new ObjectRepositoryHandler();
    public SampleTestHelper() throws Exception {
    }

    /**
     * To mobileLogin
     * This method is to perform mobile login functionality in android platform
     * @param driver
     * @author Athira CS
     * @since 22-10-2023
     */
    public void mobileLogin(WebDriver driver) {
        try {
            objr.locateElement("welcome", driver);
            objr.locateElement("add_newemail", driver);
            objr.locateElement("select_google", driver);
            Utilities.delay(2);
            objr.sendElementData("username", "athbt001", driver);
            driver.navigate().back();
            objr.locateElement("next", driver);
            Utilities.delay(2);
            objr.sendElementData("password", "Test@123", driver);
            objr.locateElement("nextbutton", driver);
            Utilities.delay(2);
            objr.locateElement("iagreebutton", driver);
            objr.locateElement("signinconsent", driver);
            objr.locateElement("accept", driver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * To WebLogin
     * This method is to perform gmail Login for the web application
     * @param driver
     * @param driver
     * @author Athira CS
     * @since 22-10-2023
     */
    public void WebLogin(WebDriver driver) throws AutomationException {
        try {
            objr.locateElement("signin", driver);
            Assert.assertTrue(objr.compareElementData("logo", driver));
            Assert.assertTrue(objr.compareElementData("signintext", driver));
            Assert.assertTrue(objr.compareElementData("continuetext", driver));
            Assert.assertTrue(objr.compareElementData("emailorphonetext", driver));
            Assert.assertTrue(objr.compareElementData("forgotemail", driver));
            Assert.assertTrue(objr.compareElementData("createaccount", driver));
            objr.sendElementData("username", "athbt001@gmail.com", driver);
            objr.locateElement("btn_login", driver);
            Utilities.delay(15);
            objr.sendElementData("password", "Test@123", driver);
            Assert.assertTrue(objr.compareElementData("logo", driver));
            Assert.assertTrue(objr.compareElementData("accountname", driver));
            Assert.assertTrue(objr.compareElementData("showpassword", driver));
            Assert.assertTrue(objr.compareElementData("profile", driver));
            Assert.assertTrue(objr.compareElementData("forgotpassword", driver));
            objr.locateElement("passnext", driver);
            Utilities.delay(3);
        } catch (Exception e) {
            Assert.fail("Assertion failed--->" + e);
            e.printStackTrace();
        }
    }

    /**
     * To ComposeEmail
     *
     * @param driver
     * @author Athira CS
     * @since 22-10-2023
     */
    public void ComposeEmail(WebDriver driver) {

        try {
            Utilities.delay(3);
            //objr.locateElement("welcome", driver);
            //Utilities.delay(2);
            objr.locateElement("takemetogmail", driver);
            objr.locateElement("close", driver);
            objr.locateElement("composeEmail", driver);
            objr.sendElementData("toaddress", "athbt001@gmail.com", driver);
            objr.locateElement("drop", driver);
            objr.sendElementData("subject", "TestSubject", driver);
            objr.sendElementData("body", "This is a body for test email", driver);
            objr.locateElement("send", driver);
            objr.locateElement("messagesent", driver);

        } catch (Exception e) {
            Assert.fail("Assertion failed--->" + e);
            e.printStackTrace();
        }
    }
    /**
     * To EmailLabelling
     * this method is to check email labelling for mobile application
     * @param driver
     * @author Athira CS
     * @since 22-10-2023
     */
    public void VerifyEmailLabelling(WebDriver driver) {

        try {
            objr.locateElement("navigationdrawer", driver);
            objr.locateElement("sendlabel", driver);
            objr.locateElement("navigationdrawer", driver);
            objr.locateElement("starredlabel", driver);
            objr.locateElement("navigationdrawer", driver);
            objr.locateElement("snoozedlabel", driver);

        } catch (Exception e) {
            Assert.fail("Assertion failed--->" + e);
            e.printStackTrace();
        }
    }
    /**
     * To WebComposeEmail
     * The purpose of this method is to perform compose email functionality for web application
     * @param driver
     * @author Athira CS
     * @since 02-11-2023
     */
    public void WebComposeEmail(WebDriver driver) {
        try {
           /* objr.locateElement("signin", driver);
            Assert.assertTrue(objr.compareElementData("signintext", driver));
            objr.sendElementData("username", "athbt001@gmail.com", driver);
            objr.locateElement("btn_login", driver);
            Thread.sleep(12000);
            objr.sendElementData("password", "Test@123", driver);
            objr.locateElement("passnext", driver);*/
            Utilities.delay(5);
            objr.locateElement("composeEmail", driver);
            Utilities.delay(2);
            objr.sendElementData("toaddress", "athbt001@gmail.com", driver);
            objr.locateElement("drop", driver);
            objr.sendElementData("subject", "TestSubject", driver);
            objr.sendElementData("body", "This is a body for test email", driver);
            objr.locateElement("send", driver);
            objr.locateElement("messagesent", driver);

        } catch (Exception e) {
            Assert.fail("Assertion failed---->" + e);
            e.printStackTrace();
        }
    }
    /**
     * To WebCreateLabel
     * The purpose of this method is to do create label functionality for web application
     * @param driver
     * @author Athira CS
     * @since 02-11-2023
     */
    public void WebCreateLabel(WebDriver driver) {
        try {
           /* objr.locateElement("signin", driver);
            Assert.assertTrue(objr.compareElementData("signintext", driver));
            objr.sendElementData("username", "athbt001@gmail.com", driver);
            objr.locateElement("btn_login", driver);
            Utilities.delay(15);
            objr.sendElementData("password", "Test@123", driver);
            objr.locateElement("passnext", driver);*/
            Utilities.delay(8);
            objr.locateElement("createlabel", driver);
            Utilities.delay(7);
            objr.compareElementData("newlabel",driver);
            objr.compareElementData("plsenterlabelname",driver);
            objr.sendElementData("enterlabelname", "testlabel", driver);
            objr.locateElement("createlabelbutton", driver);
        } catch (Exception e) {
            Assert.fail("Assertion failed---->" + e);
            e.printStackTrace();
        }
    }

    /**
     * To ChangeFonts
     * The purpose of this method is to send email body text as Bold & verify whether the body text is coming as bod or not in received email
     * @param driver
     * @author Athira CS
     * @since 02-11-2023
     */
    public void ChangeFonts(WebDriver driver) {
        try {
            Utilities.delay(5);
            objr.locateElement("composeEmail", driver);
            Utilities.delay(2);
            objr.sendElementData("toaddress", "athbt001@gmail.com", driver);
            objr.locateElement("drop", driver);
            objr.sendElementData("subject", "TestSubject", driver);
            objr.sendDataTextAsBold("body",driver,"Setting the body text format as Bold using selenium");
            Utilities.delay(5);
            objr.locateElement("send", driver);
            Utilities.delay(5);
            objr.locateElement("viewmessage", driver);
            objr.verifyBoldedText("boldtext",driver);

        } catch (Exception e) {
            Assert.fail("Assertion failed---->" + e);
            e.printStackTrace();
        }
    }
}


