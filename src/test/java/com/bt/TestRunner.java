package com.bt;

import org.testng.annotations.*;
import base.BaseClass;
import reporting.AutomationReportGenerator;

public class TestRunner extends BaseClass {

	@BeforeSuite(alwaysRun = true)
	@Parameters({ "platformName", "browser" })
	public void setup(String platformName,  @Optional String browserName)
			throws Exception {
		    AutomationReportGenerator.startReport();
			startSession(platformName, browserName);
	}

	@AfterSuite
	public void tearDownMethod() throws InterruptedException {
		AutomationReportGenerator.stopReport();
		closeSession();
	}
}