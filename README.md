# UI Automator
**UI Automator** is a test automation framework to perform gmail app testing for mobile(iOS & Android platforms) , and web applications in real time. It provides  features like test execution, test reporting for mobile and web applications. The framework includes multiple pre-built functions using which an Automation Engineer can easily perform the mobile and web app automation.

<h3>Salient features of UIAutomator</h3>
<li>Cross platform interoperability.
<br><li>Mobile UI automation.
<br><li>Web app UI Automation. Supported automation in multiple browsers such as internet explorer, chrome, firefox,Edge & Safari.User can launch any browsers with out adding any driver executables on the automation framework irrespective of browser version, OS etc.
<br><li>Support for parallel execution on different devices.
<br><li>Good reporting - framework generates HTML extent report.  


# Prerequisites
# Installations
1. Install NodeJS
2. Install Appium using NodeJS
3. Install Android Studio

# Configurations
1. Set ANDROID_HOME system variable
2. set ANDROID_HOME/tools system variable
3. set ANDROID_HOME/emulator system variable



# Steps
1. Open Android Studio -> Manage Emulators
2. Create new AVD "Pixel 7" with API version 27
3. Import project into IDE, I used InteliJ IDEA which is my favorite
4. Import dependencies using maven
5. Trigger testng_iOS.xml for iOS Automation & testng_android.xml for automation in android & testng_web.xml for webapp automation
6. Test will automatically start Appium server,Android Emulator and start executing tests
7. If this test is running from the jenkins server, add parameters as platform(Android,iOS,Web) & browser from jenkins job



